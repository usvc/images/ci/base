IMAGE_URL=usvc/ci-base

# override whatever you want in here
-include ./Makefile.properties

DATE_TIMESTAMP=$$(date +'%Y')$$(date +'%m')$$(date +'%d')

build:
	docker build --tag $(IMAGE_URL):latest .

ci.export: build
	mkdir -p ./.export
	docker save --output ./.export/image.tar $(IMAGE_URL):latest

ci.import:
	docker load --input ./.export/image.tar

test: build
	container-structure-test test \
		--verbosity debug \
		--image $(IMAGE_URL):latest \
		--config ./shared/tests/base.yml

version_alpine:
	mkdir -p ./.version
	docker run --entrypoint=cat \
		$(IMAGE_URL):${TAG} /etc/alpine-release \
		> ./.version/alpine

publish: build
	mkdir -p ./.version
	docker push $(IMAGE_URL):latest
	# usvc/ci-base:<YYYYMMDD>
	$(MAKE) utils.tag_and_push FROM=latest TO=$(DATE_TIMESTAMP)
	# usvc/ci-base:alpine-<alpine_version>
	$(MAKE) version_alpine TAG=latest
	$(MAKE) utils.tag_and_push FROM=latest TO=alpine-$$(cat ./.version/alpine)

utils.tag_and_push:
	docker tag $(IMAGE_URL):${FROM} $(IMAGE_URL):${TO}
	docker push $(IMAGE_URL):${TO}
