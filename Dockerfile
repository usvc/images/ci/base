FROM alpine:latest AS base
COPY ./shared/alpine-bootstrap.sh /usr/bin/alpine-bootstrap.sh
RUN chmod +x /usr/bin/alpine-bootstrap.sh \
  && /usr/bin/alpine-bootstrap.sh
WORKDIR /
LABEL \
  description="A base ci image with some universal tools that make ci builds a joy" \
  canonical_url="https://gitlab.com/usvc/images/ci/base" \
  license="MIT" \
  maintainer="zephinzer" \
  authors="zephinzer"
