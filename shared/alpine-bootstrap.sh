#!/bin/sh
set -x;

#
# base tools
#
apk update --no-cache && apk upgrade --no-cache;
apk add --no-cache bash ca-certificates \
  curl \
  git \
  git-fast-import \
  jq \
  make \
  openssh \
  openssl\
;

#
# semver for bumping versions
#
curl -Lo /usr/bin/semver "https://github.com/usvc/semver/releases/download/v0.3.20/semver-linux-amd64";
chmod +x /usr/bin/semver;
