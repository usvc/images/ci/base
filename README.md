# Base Image for CI use

[![pipeline status](https://gitlab.com/usvc/images/ci/base/badges/master/pipeline.svg)](https://gitlab.com/usvc/images/ci/base/commits/master)
[![dockerhub link](https://img.shields.io/badge/dockerhub-usvc%2Fci--base-blue.svg)](https://hub.docker.com/r/usvc/ci-base)

A lightweight base CI image for use in `usvc` project pipelines.

Contains tools to:
  - run scripts that rely on `/bin/bash` (instead of only `/bin/sh`)
  - download files and make cURL requests
  - perform Git operations using HTTPS and SSH
  - perform JSON manipulations
  - run `Makefile`s
  - bump version using [Git tags](https://git-scm.com/book/en/v2/Git-Basics-Tagging)

# Usage

## As a base in other Docker images

You can simply use the `FROM` directive for this:

```dockerfile
FROM usvc/ci-base:latest
```

## As a script in other Docker images

If you'd like to use another Alpine-based image but use the tools from this image, you can also use the `RUN` directive to set up your image the same way this image is set up:

```dockerfile
# ...
RUN wget -O /usr/bin/alpine-bootstrap.sh https://gitlab.com/usvc/images/ci/base/raw/master/shared/alpine-bootstrap.sh \
  && chmod +x /usr/bin/alpine-bootstrap.sh \
  && /usr/bin/alpine-bootstrap.sh \
  && rm -rf /usr/bin/alpine-bootstrap.sh
```

## GitLab CI

Use the image by specifying the `job.image` value as `"usvc/ci-base:latest"`:

```yaml
job_name:
  stage: stage_name
  image: usvc/ci-base:latest
  script:
    - ...
```

# Development Runbook

## Makefile

`make` is used to codify the common operations required to build/test/publish this image. See the [`./Makefile`](./Makefile) for details on more granular recipes.

To build all images, run `make build`.

To test all images, run `make test`.

To publish all images, run `make publish`.

## CI Configuration

Set the following variables in the environment variable configuration of your GitLab CI:

| Key | Value |
| ---: | :--- |
| DOCKER_REGISTRY_URL | URL to your Docker registry |
| DOCKER_REGISTRY_USER | Username for your registry user |
| DOCKER_REGISTRY_PASSWORD | Password for your registry user |

# License

Content herein is licensed under the [MIT license](./LICENSE).
